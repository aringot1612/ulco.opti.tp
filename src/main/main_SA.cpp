#include "../core/SimulatedAnnealing.hpp"

int main(int argc, char **argv) {
  srand((unsigned) time(0));
  if (argc == 2) {
    int parameter = atoi(argv[1]);
    SimulatedAnnealing solution;

    remove("./script/sa.csv");

    std::ofstream file;
    file.open ("./script/sa.csv", std::ios_base::app);
    file << "fitness" << "\n";
    file.close();

    std::cout << "Simulated Annealing Algorithm\nIs Running, Please wait..." << std::endl;
    solution.computeSolution(parameter);
  }
}