#include "../core/RandomSearch.hpp"

int main(int argc, char **argv) {
  srand((unsigned) time(0));

  if (argc == 2) {
    int parameter = atoi(argv[1]);
    int nruns = 1000;

    if(parameter >= 10){
      RandomSearch solution;
      std::ofstream file;
      std::vector<int> sims;

      remove("./script/rs.csv");

      for (int k = 1; k <= 10 ; k++){
        sims.push_back(k * parameter/10);
      }

      file.open ("./script/rs.csv", std::ios_base::app);
      file << "nbeval;fitness" << "\n";
      file.close();

      std::cout << "Random Search Algorithm\nIs Running, Please wait..." << std::endl;
      
      for(int i = 0; i < sims.size() ; i++){
        std::cout << "Running nSims " << sims[i] << std::endl;
        for(int j = 1 ; j <= nruns; j++){
          solution.computeSolution(sims[i]);
        }
      }
    }
  }
}