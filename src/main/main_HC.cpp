#include "../core/HillClimbing.hpp"

int main(int argc, char **argv) {
  srand((unsigned) time(0));
  if (argc == 2) {
    int parameter = atoi(argv[1]);
    HillClimbing solution;

    remove("./script/hc.csv");

    std::ofstream file;
    file.open ("./script/hc.csv", std::ios_base::app);
    file << "fitness" << "\n";
    file.close();

    std::cout << "Hill Climbing Algorithm\nIs Running, Please wait..." << std::endl;
    solution.computeSolution(parameter);
  }
}