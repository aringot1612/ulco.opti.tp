#pragma once

#include "./Solution.hpp"

class RandomSearch : public Solution {
    public:
        RandomSearch();
        void computeSolution(int parameter);
};