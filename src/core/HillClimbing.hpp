#pragma once

#include "./Solution.hpp"

class HillClimbing : public Solution {
    public:
        HillClimbing();
        void computeSolution(int parameter);
};