#include "./SimulatedAnnealing.hpp"

SimulatedAnnealing::SimulatedAnnealing() : Solution(){};

void SimulatedAnnealing::computeSolution(int nSims){
    unsigned int k = -1;
    int evalNb = 0;
    generateKnapSackRandom();
    currentFitness = findFitnessValue();
    t = generateTemperature();

    while(evalNb < nSims){
        k = rand() % knapSack.n;
        x[k] = ! x[k];
        delta = findFitnessValue() - currentFitness;

        if(delta > 0)
            currentFitness = findFitnessValue();
        else if (rand() < exp(delta/t))
            currentFitness = findFitnessValue();
        else
            x[k] = ! x[k];


        Fitness = currentFitness;
        file.open ("./script/sa.csv", std::ios_base::app);
        file << Fitness << "\n";
        file.close();
        if(evalNb % 10 == 0)
            t = generateTemperature();
        evalNb++;
    }
};

float SimulatedAnnealing::generateTemperature(){
    int fitnessInit = 0;
    int fitnessNeighbour = 0;
    int sum = 0;
    unsigned int k = -1;

    for(int i = 0 ; i < 1000 ; i++){
        generateKnapSackRandom();
        fitnessInit = findFitnessValue();
        k = rand() % knapSack.n;
        x[k] = ! x[k];
        fitnessNeighbour = findFitnessValue();
        sum += fitnessNeighbour - fitnessInit;
    }
    delta = sum / 1000;
    return (delta/log (0.2));
};