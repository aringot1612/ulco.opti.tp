#include "./RandomWalk.hpp"

RandomWalk::RandomWalk() : Solution(){};

void RandomWalk::computeSolution(int nSims){
    initKnapSack();
    currentFitness = 0;
    /** Pour chaque simulation... */
    for(int i = 0 ; i < nSims ; i++){
        /** On doit générer un sac de "voisinage". */
        generateKnapSack();
        /** On récupère la valeur du sac. */
        currentFitness = findFitnessValue();
        /** On sauvegarde la configuration également. */
        if(currentFitness > Fitness) /** Si la simulation donne lieu à une meilleure valeur qu'auparavant... */
            Fitness = currentFitness; /** On met à jour la valeur de sac max atteinte. */
    }

    file.open ("./script/rw.csv", std::ios_base::app);
    file << nSims << ";" << Fitness << "\n";
    file.close();
};

void RandomWalk::generateKnapSack(){
    int i = rand() % knapSack.n;
    x[i] = ! x[i];
};