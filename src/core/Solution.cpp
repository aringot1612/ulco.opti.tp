#include "./Solution.hpp"

Solution::Solution(){
    KnapSack knapSack;
    initKnapSack();
};

void Solution::initKnapSack(){
    Fitness = 0;
    for(int i = 0 ; i < knapSack.n; i++){
        x.push_back(rand()%2);
    }
};

double Solution::findFitnessValue(){
    /** Permet de récupèrer la valeur totale du sac. */
    std::vector<int> tmp_w(knapSack.n, 0);
    /** Permet de récupèrer le poids total du sac. */
    std::vector<int> tmp_p(knapSack.n, 0);
    /** var beta. */
    int beta = 0;
    /** Coef temporaire. */
    int coef = 0;
    /** Stock le poids final. */
    int currentWeight = 0;
    /** Stock la valeur finale. */
    int currentProfit = 0;

    /** Pour chaque equipement du sac... */
    for(int i = 0 ; i < knapSack.n ; i++){
        tmp_w[i] = x[i] * knapSack.weight[i]; /** Mise à jour de la valeur. */
        tmp_p[i] = x[i] * knapSack.profit[i]; /** Mise à jour du poids. */
        if(tmp_w[i] != 0) /** Si le poids n'est pas égal à 0... */
            coef = tmp_p[i] / tmp_w[i]; /** Calcul d'un coeficient de pénalité. */
        if(coef > beta)
            beta = coef; /** Mise à jour du coef de pénalité. */
    }

    /** Mise à jour de la valeur finale. */
    currentProfit = std::accumulate(tmp_p.begin(), tmp_p.end(), 0);
    /** Mise à jour du poids final. */
    currentWeight = std::accumulate(tmp_w.begin(), tmp_w.end(), 0);

    /** Si le poids est correct... */
    if(currentWeight <= knapSack.c)
        return currentProfit; /** La valeur du sac est mise à jour. */
    else /** Sinon... */
        return currentProfit - beta*(currentWeight-knapSack.c); /** Valeur du sac mise à jour avec pénalité. */
};

void Solution::generateKnapSackRandom(){
    for(int j = 0 ; j < knapSack.n ; j++){
        x[j] = rand()%2;
    }
};