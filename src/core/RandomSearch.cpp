#include "./RandomSearch.hpp"

RandomSearch::RandomSearch() : Solution(){};

void RandomSearch::computeSolution(int nSims){
    initKnapSack();
    currentFitness = 0;

    /** Pour chaque simulation... */
    for(int i = 0 ; i < nSims ; i++){
        /** On génère un sac aléatoire...*/
        generateKnapSackRandom();
        /** On récupère la valeur du sac. */
        currentFitness = findFitnessValue();

        if(currentFitness > Fitness) /** Si la simulation donne lieu à une meilleure valeur qu'auparavant... */
            Fitness = currentFitness; /** On met à jour la valeur de sac max atteinte. */
    }

    file.open ("./script/rs.csv", std::ios_base::app);
    file << nSims << ";" << Fitness << "\n";
    file.close();
};
