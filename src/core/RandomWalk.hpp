#pragma once

#include "./Solution.hpp"

class RandomWalk : public Solution {
    public:
        RandomWalk();
        void computeSolution(int parameter);
        void generateKnapSack();
};