#pragma once

#include "./Solution.hpp"

class SimulatedAnnealing : public Solution {
    public:
        SimulatedAnnealing();
        void computeSolution(int parameter);
        float generateTemperature();
    private:
        int delta = 0;
        float t = 0;
};