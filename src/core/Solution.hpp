#pragma once
#include "./KnapSack.hpp"

class Solution{
    protected:
        KnapSack knapSack;
        std::vector<int> x;
        std::ofstream file;
        double Fitness;
        double currentFitness;
        Solution();
        double findFitnessValue();
        void generateKnapSackRandom();
        virtual void computeSolution(int parameter){};
        virtual void generateKnapSack(){};
        void initKnapSack();
};