#include "./KnapSack.hpp"

KnapSack::KnapSack(){
    std::vector<std::string> lines(4, "");
    std::ifstream myFile("./data/ks_1000.dat");

    if(myFile.is_open())  //On teste si tout est OK
    {
        /** Récupèration du nombre d'objets. */
        getline(myFile, lines[0]);

        /** Récupèration des profits. */
        getline(myFile, lines[1]);

        /** Récupèration des poids. */
        getline(myFile, lines[2]);

        /** Récupèration du poids max. */
        getline(myFile, lines[3]);

        /** Fermeture du fichier. */
        myFile.close();

        /** Conversion string -> int pour le nombre d'objets. */
        n = stoi(lines[0]);

        /** Conversion string -> double pour le poids max. */
        c = stod(lines[3]);

        /** Création d'une istringstream pour la conversion d'une string en vecteur d'entiers. */
        std::istringstream profit_string(lines[1]);
        std::istringstream weight_string(lines[2]);
        
        /** Conversion des string de poids et profits en liste d'entiers. */
        profit.assign(std::istream_iterator<int>(profit_string), std::istream_iterator<int>());
        weight.assign(std::istream_iterator<int>(weight_string), std::istream_iterator<int>());
    }
    else
        std::cout << "ERREUR: Impossible d'ouvrir le fichier." << std::endl;
}