#include "HillClimbing.hpp"

HillClimbing::HillClimbing() : Solution(){};

void HillClimbing::computeSolution(int maxEval){
    /** On doit generer une première version du sac. */
    generateKnapSackRandom();
    /** bit à switch */
    unsigned int i = -1;
    // Meilleur bit pour HillClimbing
    int iBest = -1;
    // Meilleur fitness du voisinage
    int bestNeighbor = 0;
    currentFitness = findFitnessValue();
    int evalNb = 0;

    while(evalNb < maxEval){
        for(int i = 0 ; i < knapSack.n; i++){
            /** On explore tout le voisinage */
            /** Bit switch */
            x[i] = ! x[i];

            if(iBest < 0 || bestNeighbor < findFitnessValue()){
                bestNeighbor = findFitnessValue();
                iBest = i;
            }
            /** Bit RollBack */
            x[i] = ! x[i];
        }
        if (findFitnessValue() < bestNeighbor) {
            x[iBest] = ! x[iBest];
        }
        Fitness = bestNeighbor;
        file.open ("./script/hc.csv", std::ios_base::app);
        file << Fitness << "\n";
        file.close();
        evalNb++;
    }
};