#pragma once

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <numeric>
#include <algorithm>
#include <time.h>
#include <math.h>

class KnapSack{
    public:
        KnapSack();
        int n;
        std::vector<int> profit;
        std::vector<int> weight;
        double c;    
};